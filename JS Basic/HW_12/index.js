"use strict";

const buttons = document.querySelectorAll('.btn');
document.addEventListener("keydown", event => {
    let keyCode = event.key;
    for (let btn of buttons) {
        if(btn.textContent.toLowerCase() === keyCode.toLowerCase()){
            for (const btn of buttons) {
                btn.classList.remove("color");
            };
            btn.classList.add("color");
        };
    };
});

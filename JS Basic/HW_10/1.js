let centeredContent = function () {
    let tabTit =document.querySelectorAll('.tabs-title'),
    tabCon = document.querySelectorAll('.tab'),
    tabName;
    tabTit.forEach(item=>{
        item.addEventListener('click', selectTabTit)
    });
    function selectTabTit() {
        tabTit.forEach(item=>{
            item.classList.remove('active')
        })
        this.classList.add('active')
        // console.log(this);
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
        tabCon.forEach(item=>{
            item.classList.contains(tabName)? item.classList.add('active'): item.classList.remove('active')
        })
    }
};


centeredContent();
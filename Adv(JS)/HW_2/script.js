const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж i м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const div = document.querySelector('#root');

let neededInfo = ['author','name','price'];

books.forEach((book) => {
try{
        neededInfo.forEach((prop) => {
          if(!book[prop]) {
            throw new Error(`This book dont have ${prop}`);
          }
        }) 

        const ul = document.createElement('ul');
        for (prop in book) {
          const li = document.createElement('li');
          li.innerText = `${prop}: ${book[prop]}`
          ul.append(li)
        }
        div.append(ul)
      }
    catch(error) {  
        console.log(error)
    }
  })
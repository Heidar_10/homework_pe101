let btn = document.querySelector('#btn-ip');

let ip = document.querySelector("#ip")
let continent = document.querySelector("#continent")
let country = document.querySelector("#country")
let city = document.querySelector("#city")
let regionName = document.querySelector("#regionName")
let district = document.querySelector('#district')

btn.addEventListener('click', () => {
    async function showIP(){
        let responsee = await fetch('https://api.ipify.org/?format=json');
        let getIP = await responsee.json()
        console.log(getIP.ip);
        await fetch(`http://ip-api.com/json/${getIP.ip}?fields=continent,country,region,regionName,city,query,district`)
        .then((response) => response.json())
        .then((response) => {
            continent.append(`${response.continent} ;`);
            ip.append(`${response.query} ;`);
            country.append(`${response.country} ;`);
            city.append(`${response.city} ;`);
            regionName.append(`${response.regionName} ;`);
            district.append(`${response.district ? district : "Не знайдено район"} .`);
        })
    }
    showIP()
})

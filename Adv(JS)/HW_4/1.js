const list = document.querySelector("#root>ul");
fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((response) => {
    return response.json();
  })
  .then((films) => {
    films.forEach(film => {
    const li = document.createElement("li");
      li.innerHTML = `
      <h2>${film.name}</h2>
      <h3>Episode: ${film.episodeId}</h3>
      <p>${film.openingCrawl}</p>
      <p>Actors:</p>
      `
      const charactersFetch = [];
      film.characters.forEach(character =>{
          charactersFetch.push(
            fetch(character).then(res =>{
              return res.json();
            })

          );
      })
      Promise.all(charactersFetch).then((characterList)=>{
        const actorsList = document.createElement("ul")
        characterList.forEach(({name})=>{
            const actorLi = document.createElement('li')
            actorLi.textContent = name;
          actorsList.append(actorLi)
        })
      
        console.log(actorsList)
        li.append(actorsList)

      });

      
    list.append(li)
  });

});


  

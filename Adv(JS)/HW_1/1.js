class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
   get name(){
    return this._name;
   }
   set name(value) {
    this._name = value;
  }

   get age(){
    return this._age;
   }

   set age(value) {
    this._age = value;
  }

   get salary(){
    return this._salary;
   }

   set salary(value) {
    this._salary = value;
  }

}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }
    get lang(){
        return this._lang
    }
    set lang(value) {
        this._lang = value;
    }
    
    get infoSalary(){
        return  this._salary * 3
    }

}
let employee = new Employee('David', 25 , 200)
console.log(employee);

let programmer1 = new Programmer('Jack', 20, 500, 'english')
console.log(programmer1);
console.log(programmer1.name);
console.log(programmer1.age);
console.log(programmer1.infoSalary);
console.log(programmer1.lang);

let programmer2 = new Programmer('Ivan', 29, 900, 'ukrainian')
console.log(programmer2);
console.log(programmer2.name);
console.log(programmer2.age);
console.log(programmer2.infoSalary);
console.log(programmer2.lang);
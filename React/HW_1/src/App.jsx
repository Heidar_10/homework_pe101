import React, { useState } from "react";
import Modal from './components/Modal/Modal.jsx';
import Button from "./components/Button/Button.jsx";

export default class App extends React.Component {
  
    state = {firstModal:false, seconModal:false}
    openFirstModal = ()=>{
      this.setState({
        firstModal: !this.state.firstModal
        
      })
    }
    openSecondModal = ()=>{
      this.setState({
        seconModal: !this.state.seconModal
        
      })
    }

    render(){    
      const {firstModal, seconModal,} = this.state;

      return(            
      <React.Fragment>
        <div className="button">
          <Button onClick = {this.openFirstModal} text={'Open first modal'}  backgroundColor={'red'}/>
          <Button onClick = {this.openSecondModal}  text={'Open second modal'} backgroundColor={'green'}/>
        </div>           
            {firstModal && <Modal onClick = {this.openFirstModal}  backgroundColorTitle={'red'} backgroundColorText={'tomato'} header= {'Do you want to delete this file?'} text = {'Once you delete this file, it wont be possible to undo this action.  Are you sure you want to delete it?'} actions={<><Button backgroundColor={'red'} onClick = {this.openFirstModal} text = {'Ok'}/><Button backgroundColor={'green'} onClick = {this.openFirstModal} text={'Cancel'} /></>} closeButton/>}
            {seconModal && <Modal onClick = {this.openSecondModal} backgroundColorTitle={'forestgreen'} backgroundColorText={'cornflowerblue'} header= {'Do you want to save this file?'} text = {'If you save this file, can you change it later on the site?'} actions={<><Button backgroundColor={'red'} onClick = {this.openSecondModal} text = {'Ok'}/><Button backgroundColor={'green'} onClick = {this.openSecondModal} text={'Cancel'} /></>} closeButton/>}
      </React.Fragment>
      )
    }
}

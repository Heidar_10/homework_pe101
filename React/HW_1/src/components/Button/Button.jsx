import React from "react";
import "./Button.scss"

export default class Button extends React.Component{
    render(){
        const {onClick, text, backgroundColor} = this.props;

        return(
            <div>
                <button className="button-modal" style={{backgroundColor:backgroundColor}} onClick={onClick} >{text} </button>
            </div>             
        )
    }
}
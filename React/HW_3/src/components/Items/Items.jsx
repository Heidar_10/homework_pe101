import React, {useState,useEffect} from "react"
import './Items.scss'
import Item from "./Item"
const Items =({openSecondModal, onAdd, onFavorite,onDelete})=>{
  const [sendRequest,setSendRequest] = useState([])
  
  useEffect(() => {
    fetch("data.json")
    .then(response =>response.json())
    .then(result => setSendRequest (result))
  },[]);


    return (
     <main className="main">
        {sendRequest.map(el =>(
                <>
                  <Item
                  key={el.id} 
                  item = {el}
                  openSecondModal = {openSecondModal} 
                  onAdd = {onAdd} 
                  onFavorite={onFavorite} 
                  onDelete={onDelete}
                  />   
                                 
                </>
        ))}
        
     </main>
    )
  }

  export default Items

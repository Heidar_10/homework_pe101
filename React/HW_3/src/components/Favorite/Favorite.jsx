import React, { useState,useEffect } from 'react';
import './Favorite';
import PropTypes from 'prop-types';
import Item from '../Items/Item';

const Favorite = ({ onFavorite, openSecondModal,onAdd,onDelete }) => {
  const [colorStar, setColorStar] = useState('black');
  const [storedCards, setStoredCards] = useState(JSON.parse(localStorage.getItem('Favorite'))|| []);
  useEffect(()=> {
    setStoredCards(JSON.parse(localStorage.getItem('Favorite'))|| [])
  }, [localStorage.getItem('Favorite')])
    
  const onClickStar = () => {
    const color = colorStar;
    console.log(color);

    if (color === 'black') {
      setColorStar('yellow');
    } else if (color === 'yellow') {
      setColorStar('black');
    }
  };
  const [sendRequest,setSendRequest] = useState([])  
  useEffect(() => {
    fetch("data.json")
    .then(response =>response.json())
    .then(result => setSendRequest (result))
    
  },[]);

  return (
    <>
    <main className="main">
        {storedCards && storedCards.map(el =>(
            <>
                
                  <Item 
                  key={el.id} 
                  item = {el}
                  openSecondModal = {openSecondModal} 
                  onAdd = {onAdd} 
                  onFavorite={onFavorite} 
                  onDelete={onDelete}
                  />     
      
            </>
        ))}
        
     </main>
    </>
    
  );
};
Favorite.propTypes = {
  onClick: PropTypes.func ,
  img:PropTypes.object,
  name:PropTypes.string,
  price:PropTypes.number,
  color:PropTypes.string, 
  article:PropTypes.string,
  openSecondModal:PropTypes.func
};
export  {Favorite}

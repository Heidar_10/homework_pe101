import React from 'react';
import { Routes, Route} from "react-router-dom";
import Modal from './components/Modal/Modal.jsx';
import Button from "./components/Button/Button.jsx";
import {Header} from "./components/Header/Header.jsx";
import Items from "./components/Items/Items.jsx";
import './index.css'
import {Favorite} from "./components/Favorite/Favorite.jsx";
import {Buy} from "./components/Buy/Buy.jsx"


export default class App extends React.Component {
    state = {
      firstModal:false, 
      seconModal:false, 
      favorite:[...JSON.parse(localStorage.getItem('Favorite')) || []],
      orders:[...JSON.parse(localStorage.getItem('Buy')) || []],
      ordersCurrent:{},
      favoriteCurrent:{},
      deleteCurrent:{}
    }
    
    
    
    addToOrder = this.addToOrder.bind(this)
    addToFavorite = this.addToFavorite.bind(this)
    deleteOrder = this.deleteOrder.bind(this)
    openFirstModal = (item)=>{
      this.setState({
        firstModal: !this.state.firstModal,
        deleteCurrent: item,        
      })
    }
    openSecondModal = (item)=>{
      this.setState({
        seconModal: !this.state.seconModal,
        ordersCurrent: item,        
        favoriteCurrent: item,        
      })
    }
    handelFavorite = (obj)=>{
        this.setState((pref)=>{
            return {...pref, favorite : [...pref.favorite, obj]}
        })
    }
    handelBuy = (obj)=>{
        this.setState((pref)=>{
            return {...pref, orders : [...pref.orders, obj]}
        })
    }
   
    addToOrder(item){
      this.setState({orders:[...this.state.orders,item]}) 
      const local = JSON.parse(localStorage.getItem("Buy") )|| []
      localStorage.setItem("Buy", JSON.stringify([...local,item]))
    }    

    addToFavorite(item) {
      const { favorite } = this.state;
    
      const existingItem = favorite.find((el) => el.id === item.id);
      if (existingItem) {
        const updatedFavorite = favorite.filter((el) => el.id !== item.id);
        this.setState({ favorite: updatedFavorite });
        localStorage.setItem("Favorite", JSON.stringify(updatedFavorite));
      } else {
        const updatedFavorite = [...favorite, item];
        this.setState({ favorite: updatedFavorite });
        localStorage.setItem("Favorite", JSON.stringify(updatedFavorite));
      }
    }
    
    deleteOrder(id){
      this.setState({orders:[...this.state.orders.filter(el => el.id !== id)]})
      localStorage.setItem("Buy", JSON.stringify([...this.state.orders.filter(el => el.id !== id)]))             
    }

   

    render(){   
       
      const {firstModal, seconModal ,favorite,orders } = this.state;     
      const countFavorite = favorite.length;
      const countBuy = orders.length

      return(            
      <React.Fragment>
          <Routes>
            
            <Route path="/" element={<Header  count = {countFavorite} countBuy = {countBuy} onClick={this.handelFavorite}/> }>
                <Route index element={<Items  onAdd={this.addToOrder} onFavorite={this.addToFavorite}  openSecondModal={this.openSecondModal} onDelete = {this.deleteOrder}/>}/>
                <Route path="buy" element={<Buy onAdd={this.addToOrder} onFavorite={this.addToFavorite}  openSecondModal={this.openSecondModal}  onDelete = {this.deleteOrder} openFirstModal={this.openFirstModal}/>}/>
                <Route path="favorite" element={<Favorite onAdd={this.addToOrder} onFavorite={this.addToFavorite}  openSecondModal={this.openSecondModal}/>}/>
                <Route/>
            </Route>
          </Routes>

          {firstModal && <Modal onClick = {this.openFirstModal}  backgroundColorTitle={'red'} backgroundColorText={'tomato'} 
          header= {'Do you want to delete this phone?'} text = {'Once you delete this phone, it wont be possible to undo this action.  Are you sure you want to delete it?'} 
          actions={
          <>
            <Button backgroundColor={'red'}  onDelete = {this.deleteOrder} onClick = {()=>{this.openFirstModal();
              this.deleteOrder(this.state.deleteCurrent)}}  text = {'Ok'}/>
            <Button backgroundColor={'green'} onClick = {this.openFirstModal} text={'Cancel'} />
          </>} closeButton/>}

          {seconModal && <Modal onClick = {this.openSecondModal}  backgroundColorTitle={'forestgreen'} backgroundColorText={'cornflowerblue'} 
              header= {'Confirmation'} text = {'Are you sure you want to add to cart?'} 
              actions={
            <>
              <Button backgroundColor={'red'}  onAdd ={this.addToOrder}  onClick={()=>{ this.openSecondModal(); 
              this.addToOrder(this.state.ordersCurrent)}}  text = {'Ok'}/>
              <Button backgroundColor={'green'} onClick = {this.openSecondModal} text={'Cancel'} />
            </>
          }  closeButton/>}
      </React.Fragment>

    
      )
    }
   
}


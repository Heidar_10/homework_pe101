import React ,{useEffect, useState} from 'react';
import './Buy';
import PropTypes from 'prop-types';
import Item from '../Items/Item';
const Buy = ({  onFavorite, openSecondModal,openFirstModal,onAdd , onDelete}) => {
  // const data = useSelector((state) => state.data.localStorage.getItem('Buy')); // получение данных из Redux Store
  const [storedCards, setStoredCards] = useState(JSON.parse(localStorage.getItem('Buy'))|| []);

  useEffect(()=> {
    setStoredCards(JSON.parse(localStorage.getItem('Buy'))|| [])
  }, [JSON.parse(localStorage.getItem('Buy'))|| []])  

  // useEffect(() => {
  //   const storedData = JSON.parse(localStorage.getItem('Buy')) || [];
  //   setStoredCards(storedData);
  // }, []);
  
  // const storedCards = JSON.parse(localStorage.getItem('Buy')) || [];
  console.log(storedCards);   
  return (
    <>
        <main className="main">
         {storedCards && storedCards.map(el =>(
                <>
                    <Item 
                    key={el.id} 
                    item = {el}
                    openSecondModal = {openSecondModal} 
                    openFirstModal = {openFirstModal}
                    onAdd = {onAdd} 
                    onFavorite={onFavorite} 
                    onDelete={onDelete}
                    /> 
                </>
            ))
            
            }
        </main>
    </>
    
  );
};
Buy.propTypes = {
  onClick: PropTypes.func ,
  img:PropTypes.object,
  name:PropTypes.string,
  price:PropTypes.number,
  color:PropTypes.string, 
  article:PropTypes.string,
  openSecondModal:PropTypes.func,
  openFirstModal:PropTypes.func
};
export {Buy}
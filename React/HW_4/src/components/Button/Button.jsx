import React from "react";
import "./Button.scss"

const Button =({onClick, text, backgroundColor,addToOrder,deleteOrder})=> {

        return(
            <div>
                <button className="button-modal" style={{backgroundColor:backgroundColor}} onClick={onClick} onAdd={addToOrder}  onDelete={deleteOrder}>{text}</button>
            </div>             
        )
    
}

export default Button

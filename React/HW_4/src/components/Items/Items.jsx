import React, {useEffect} from "react"
import { useSelector } from 'react-redux';
import './Items.scss'
import Item from "./Item"
import { fetchCards, data } from '../../cardsSlice';
import { useDispatch } from 'react-redux';

const Items =({openSecondModal, onAdd, onFavorite,onDelete})=>{
  const dispatch = useDispatch();
  const item = useSelector(data);
console.log(item);
  useEffect(() => {
    dispatch(fetchCards());
  }, [dispatch]);
  return (
    <main className="main">
        {item.map(el =>(
                <>
                  <Item
                  key={el.id} 
                  item = {el}
                  openSecondModal = {openSecondModal} 
                  onAdd = {onAdd} 
                  onFavorite={onFavorite} 
                  onDelete={onDelete}
                  />   
                                 
                </>
        ))}
        
     </main>
    )
  }

  export default Items

import { setData } from "./dataSlice";
import axios from "axios";

export const fetchData = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get("data.json");
      const data = response.data;
      dispatch(setData(data));
      console.log(data);
    } catch (error) {}
  };
};

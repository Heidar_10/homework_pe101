import React, { useState,useEffect } from 'react';
import { Routes, Route} from "react-router-dom";
import Modal from './components/Modal/Modal.jsx';
import Button from "./components/Button/Button.jsx";
import {Header} from "./components/Header/Header.jsx";
import Items from "./components/Items/Items.jsx";
import './index.css'
import {Favorite} from "./components/Favorite/Favorite.jsx";
import {Buy} from "./components/Buy/Buy.jsx"
import { useDispatch } from 'react-redux';
import { openModal, closeModal } from './modalSlice';
const App = () => {

  const dispatch = useDispatch();

  const [firstModal, setFirstModal] = useState(false);
  const [secondModal, setSecondModal] = useState(false);
  const [favorite, setFavorite] = useState(
    [...JSON.parse(localStorage.getItem("Favorite")) || []]
  );
  const [orders, setOrders] = useState(
    [...JSON.parse(localStorage.getItem("Buy")) || []]
  );
  const [ordersCurrent, setOrdersCurrent] = useState({});
  const [favoriteCurrent, setFavoriteCurrent] = useState({});
  const [deleteCurrent, setDeleteCurrent] = useState({});

  const countFavorite = favorite.length;
  const countBuy = orders.length;

  useEffect(() => {
    localStorage.setItem("Favorite", JSON.stringify(favorite));
  }, [favorite]);

  useEffect(() => {
    localStorage.setItem("Buy", JSON.stringify(orders));
  }, [orders]);

  const addToOrder = (item) => {
    setOrders([...orders, item]);
  };

  const addToFavorite = (item) => {
    const existingItem = favorite.find((el) => el.id === item.id);
    if (existingItem) {
      const updatedFavorite = favorite.filter((el) => el.id !== item.id);
      setFavorite(updatedFavorite);
    } else {
      const updatedFavorite = [...favorite, item];
      setFavorite(updatedFavorite);
    }
  };

  const deleteOrder = (id) => {
    setOrders(orders.filter((el) => el.id !== id));
  };
  const handleCheckout = (id) => {
    setOrders(orders.filter((el) => el.id !== id));
  };

  const openFirstModal = (item) => {
    setFirstModal(!firstModal);
    setDeleteCurrent(item);
    dispatch(openModal());
  };

  const closeFirstModal = ()=>{
    setFirstModal(!firstModal);
    dispatch(closeModal());
  }

  const openSecondModal = (item) => {
    setSecondModal(!secondModal);
    setOrdersCurrent(item);
    setFavoriteCurrent(item);
    dispatch(openModal());

  };

  const closeSecondModal = ()=>{
    setSecondModal(!secondModal);
    dispatch(closeModal());
  }

  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <Header
              count={countFavorite}
              countBuy={countBuy}
              onClick={addToFavorite}
            />
          }
        >
          <Route
            index
            element={
              <Items
                onAdd={addToOrder}
                onFavorite={addToFavorite}
                openSecondModal={openSecondModal}
                onDelete={deleteOrder}
              />
            }
          />
          <Route
            path="buy"
            element={
              <Buy
                onAdd={addToOrder}
                onFavorite={addToFavorite}
                openSecondModal={openSecondModal}
                onDelete={deleteOrder}
                openFirstModal={openFirstModal}
              />
            }
          />
          <Route
            path="favorite"
            element={
              <Favorite
                onAdd={addToOrder}
                onFavorite={addToFavorite}
                openSecondModal={openSecondModal}
              />
            }
          />
        </Route>
      </Routes>
      {firstModal && (
          <Modal
          onClick={openFirstModal}
          backgroundColorTitle={"red"}
          backgroundColorText={"tomato"}
          header={"Do you want to delete this phone?"}
          text={
            "Once you delete this phone, it won`t be possible to undo this action. Are you sure you want to delete it?"
          }
          actions={
            <>
              <Button
                backgroundColor={"red"}
                onClick={() => {
                  closeFirstModal();
                  deleteOrder(deleteCurrent.id);
                  handleCheckout(deleteCurrent.id)
                  
                }}
                text={"Ok"}
              />
              <Button
                backgroundColor={"green"}
                onClick={closeFirstModal}
                text={"Cancel"}
              />
            </>
          }
          closeButton
        />
      )}

      {secondModal && (
        <Modal
          onClick={openSecondModal}
          backgroundColorTitle={"forestgreen"}
          backgroundColorText={"cornflowerblue"}
          header={"Confirmation"}
          text={"Are you sure you want to add to cart?"}
          actions={
            <>
              <Button
                backgroundColor={"red"}
                onClick={() => {
                  closeSecondModal();
                  addToOrder(ordersCurrent);
                }}
                text={"Ok"}
              />
              <Button
                backgroundColor={"green"}
                onClick={closeSecondModal}
                text={"Cancel"}
                
              />
            </>
          }
          closeButton
        />
      )} 
    </>
  );
};

export default App;
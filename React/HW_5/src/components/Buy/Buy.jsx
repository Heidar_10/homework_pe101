import React, { useState, useEffect } from 'react';
import './Buy.scss';
import PropTypes from 'prop-types';
import ItemBuy from './ItemBuy';
import { Formik, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';
import { PatternFormat } from 'react-number-format';

const Buy = ({ openFirstModal, onDelete }) => {
  const [storedCards, setStoredCards] = useState(JSON.parse(localStorage.getItem('Buy')) || []);
  const [formSubmitted, setFormSubmitted] = useState(false);

  useEffect(() => {
    setStoredCards(JSON.parse(localStorage.getItem('Buy')) || []);
  }, []);

  const validationsSchema = yup.object().shape({
    name: yup.string().typeError('Must be a string!!!').required('Fill in!'),
    surname: yup.string().typeError('Must be a string!!!').required('Fill in!'),
    address: yup.string().typeError('Enter the correct address!!!').required('Fill in!'),
    phone: yup.string().typeError('Enter the correct phone number!!!').required('Fill in!'),
  });

  const handleCheckout = (values) => {
    console.log('Замовлення оформлене! Інформація про покупця:', values);
    console.log('Куплені товари:', storedCards);
    localStorage.removeItem('Buy');
    setStoredCards([]);
    setFormSubmitted(true);
  };

  return (
    <>
      {storedCards.length > 0 && !formSubmitted && (
        <Formik
          initialValues={{
            name: '',
            surname: '',
            age: '',
            address: '',
            phone: '',
          }}
          validateOnBlur
          onSubmit={(values, { setSubmitting, setTouched }) => {
            setTouched(values);
            handleCheckout(values);
            setSubmitting(false);
          }}
          validationSchema={validationsSchema}
        >
          {({ values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty }) => (
            <form className={`form`} onSubmit={handleSubmit}>
              {/* name */}
              <div className='input-container'>
                <label htmlFor={`name`} className={`label`}>
                  Name
                </label>
                <Field className={'input'} type={`text`} name={`name`} onChange={handleChange} onBlur={handleBlur} value={values.name}/>
                <ErrorMessage name="name" component="p" className={'error'} touched={touched.name} />
              </div>

              {/* surname */}
              <div className='input-container'>
                <label htmlFor={`surname`} className={`label`}>
                  Surname
                </label>
                <Field className={'input'} type={`text`} name={`surname`} onChange={handleChange} onBlur={handleBlur} value={values.surname}/>
                <ErrorMessage name="surname" component="p" className={'error'} touched={touched.surname} />
              </div>

              {/* age */}
              <div className='input-container'>
                <label htmlFor={`age`} className={`label`}>
                  Age
                </label>
                <Field className={'input'} type={`number`} name={`age`} onChange={handleChange} onBlur={handleBlur} value={values.age}/>
                <ErrorMessage name="age" component="p" className={'error'} touched={touched.age} />
              </div>

              {/* address */}
              <div className='input-container'>
                <label htmlFor={`address`} className={`label`}>
                  Delivery address
                </label>
                <Field className={'input'} type={`text`} name={`address`} onChange={handleChange} onBlur={handleBlur} value={values.address}/>
                <ErrorMessage name="address" component="p" className={'error'} touched={touched.address} />
              </div>

              {/* phone */}
              <div className='input-container'>
                <label htmlFor={`phone`} className={`label`}>
                  Phone
                </label>
                <Field name="phone">
                  {({ field }) => (
                    <PatternFormat
                      {...field}
                      className={'input'}
                      format="+38 (###) ###-##-##"
                      mask="_"
                      placeholder="+38 (___) ____-___"
                    />
                  )}
                </Field>
                <ErrorMessage name="phone" component="p" className={'error'} touched={touched.phone} />
              </div>

              <button
                disabled={!isValid || Object.keys(touched).length === 0}
                className='button-submit'
                type={`submit`}
              >
                Confirm order
              </button>
            </form>
          )}
        </Formik>
      )}

      <main className="main-purchase">
        <div></div>
        <div className='cards'>
          {storedCards &&
            storedCards.map((el) => (
              <ItemBuy key={el.id} item={el} openFirstModal={openFirstModal} onDelete={onDelete} />
            ))}
        </div>
      </main>
    </>
  );
};

Buy.propTypes = {
  onFavorite: PropTypes.func,
  openSecondModal: PropTypes.func,
  openFirstModal: PropTypes.func,
  onAdd: PropTypes.func,
  onDelete: PropTypes.func,
};

export { Buy };

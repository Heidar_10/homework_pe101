import React from 'react';
import PropTypes from 'prop-types';

const ItemBuy = ({  item, openFirstModal , onDelete}) => { 
  return (
    <div className="item" id={item.id} >
      <img className="item__img" src={'./img/' + item.img} alt="" />
      <h2 className="item__name">{item.name}</h2>
      <b className="item__price">{item.price}</b>
      <p className="item__color">Color: {item.color}</p>
      <p className="item__article" >Article: {item.article}</p>
      <div className="icons" style={{marginBottom:'10px'}}>
        <svg xmlns="http://www.w3.org/2000/svg" onClick={()=>{openFirstModal(item)}}  width="24" height="24" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"> 
        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/> <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/> </svg>
      </div>
    </div>
  );
};
ItemBuy.propTypes = {
  onClick: PropTypes.func ,
  img:PropTypes.object,
  name:PropTypes.string,
  price:PropTypes.number,
  color:PropTypes.string, 
  article:PropTypes.string,
  openSecondModal:PropTypes.func
};
export default ItemBuy
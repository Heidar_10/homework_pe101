import React, { useState, useEffect } from 'react';
import './Favorite';
import PropTypes from 'prop-types';
import ItemFavorite from './ItemFavorite';

const Favorite = ({ onFavorite, openSecondModal, onAdd, onDelete }) => {
  const [colorStar, setColorStar] = useState(
    localStorage.getItem('colorStar') || 'black'
  );
  const [storedCards, setStoredCards] = useState(
    JSON.parse(localStorage.getItem('Favorite')) || []
  );

  useEffect(() => {
    setStoredCards(JSON.parse(localStorage.getItem('Favorite')) || []);
  }, [JSON.parse(localStorage.getItem('Favorite')) || []]);

  useEffect(() => {
    localStorage.setItem('colorStar', colorStar);
  }, [colorStar]);

  const onClickStar = () => {
    if (colorStar === 'yellow') {
      setColorStar('yellow');
    } else {
      setColorStar('black');
    }
  };
  
  useEffect(() => {
    localStorage.setItem('colorStar', colorStar);
  }, [colorStar]);

  const [sendRequest, setSendRequest] = useState([]);
  useEffect(() => {
    fetch('data.json')
      .then((response) => response.json())
      .then((result) => setSendRequest(result));
  }, []);

  return (
    <>
      <main className="main">
        {storedCards &&

          
          storedCards.map((el) => (
            <ItemFavorite
              key={el.id}
              item={el}
              openSecondModal={openSecondModal}
              onAdd={onAdd}
              onFavorite={onFavorite}
              onDelete={onDelete}
              colorStar={colorStar}
              onClickStar={onClickStar}
            />
          ))}
      </main>
    </>
  );
};

Favorite.propTypes = {
  onClick: PropTypes.func,
  img: PropTypes.object,
  name: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  article: PropTypes.string,
  openSecondModal: PropTypes.func,
};

export { Favorite };

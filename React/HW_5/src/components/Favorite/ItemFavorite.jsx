import React, { useState } from 'react';
import '../Items/Items';
import PropTypes from 'prop-types';

const ItemFavorite = ({  item, onFavorite}) => {
  const [colorStar, setColorStar] = useState('black');

  const onClickStar = () => {
    const color = colorStar;
  
    if (color === 'black') {
      setColorStar('yellow');
      localStorage.setItem('colorStar', 'yellow');
    } else if (color === 'yellow') {
      setColorStar('black');
      localStorage.setItem('colorStar', 'black');
    }
  };

  

  return (
    <div className="item" id={item.id} >
      <img className="item__img" src={'./img/' + item.img} alt="" />
      <h2 className="item__name">{item.name}</h2>
      <b className="item__price">{item.price}</b>
      <p className="item__color">Color: {item.color}</p>
      <p className="item__article">Article: {item.article}</p>
      <div className="icons">
        <svg style={{marginBottom:'10px'}}
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => {
            onClickStar();
            onFavorite(item);
          }}
          // fill={localStorage.setItem('colorStar', colorStar)}
          fill={localStorage.setItem('colorStar', colorStar)}
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
        </svg>
      </div>
    </div>
  );
};
ItemFavorite.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    color: PropTypes.string,
    article: PropTypes.string,
  }),
  onFavorite: PropTypes.func,
};
export default ItemFavorite
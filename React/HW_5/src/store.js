import { configureStore } from "@reduxjs/toolkit";
import dataReducer from "./dataSlice";
import modalReducer from "./modalSlice";
import cardsReducer from "./cardsSlice";

const store = configureStore({
  reducer: {
    data: dataReducer,
    modal: modalReducer,
    cards: cardsReducer,
  },
});

export default store;

import React from "react";
import Modal from './components/Modal/Modal.jsx';
import Button from "./components/Button/Button.jsx";
import Header from "./components/Header/Header.jsx";
import Items from "./components/Items/Items.jsx";
import './index.css'

export default class App extends React.Component {
  
  
    state = {
      firstModal:false, 
      seconModal:false, 
      favorite:[...JSON.parse(localStorage.getItem('Favorite')) || []],
      orders:[...JSON.parse(localStorage.getItem('Buy')) || []],
      ordersCurrent:{},
      favoriteCurrent:{}
    }

    addToOrder = this.addToOrder.bind(this)
    addToFavorite = this.addToFavorite.bind(this)
    openFirstModal = ()=>{
      this.setState({
        firstModal: !this.state.firstModal
        
      })
    }
    openSecondModal = (item)=>{
      this.setState({
        seconModal: !this.state.seconModal,
        ordersCurrent: item,        
        favoriteCurrent: item,        
      })
    }
    handelFavorite = (obj)=>{
        this.setState((pref)=>{
            return {...pref, favorite : [...pref.favorite, obj]}
        })
    }
    handelBuy = (obj)=>{
        this.setState((pref)=>{
            return {...pref, orders : [...pref.orders, obj]}
        })
    }
   
    addToOrder(item){
      this.setState({orders:[...this.state.orders,item]}) 
      const local = JSON.parse(localStorage.getItem("Buy") )|| []
      localStorage.setItem("Buy", JSON.stringify([...local,item]))
    }    

    addToFavorite(item){
      this.setState({favorite:[...this.state.favorite, item]})
      const local = JSON.parse(localStorage.getItem("Favorite") )|| []
      localStorage.setItem("Favorite", JSON.stringify([...local,item]))
    }

    render(){    
      const {firstModal, seconModal,handelFavorite ,favorite,count,orders, onAdd,onFavorite,item } = this.state;     
      const countFavorite = favorite.length;
      const countBuy = orders.length
      console.log(favorite);
      console.log(this.state);
      return(            
      <React.Fragment>
        <Header  count = {countFavorite} countBuy = {countBuy} onClick={this.handelFavorite}/>        
        <Items items={this.state.items} onAdd={this.addToOrder} onFavorite={this.addToFavorite}  openSecondModal={this.openSecondModal} />

        {seconModal && <Modal onClick = {this.openSecondModal}  backgroundColorTitle={'forestgreen'} backgroundColorText={'cornflowerblue'} 
        header= {'Confirmation'} text = {'Are you sure you want to add to cart?'} 
        actions={
            <>
              <Button backgroundColor={'red'} items={this.state.items} onAdd ={this.addToOrder}  onClick={()=>{ this.openSecondModal(); this.addToOrder(this.state.ordersCurrent)}}  text = {'Ok'}/>
              <Button backgroundColor={'green'} onClick = {this.openSecondModal} text={'Cancel'} />
            </>
          }  closeButton/>} 


      
      </React.Fragment>
      )
    }
   
}



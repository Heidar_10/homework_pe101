import React, { useState } from "react";
import PropTypes from "prop-types"
import Button from "../Button/Button";
import "./Modal.scss";
import App from "../../App";

export default class Modal extends React.Component {

  render() {
    const { header, text, actions, closeButton, onClick, backgroundColorTitle, backgroundColorText,} = this.props;
 
    return (
    <>

      <div className="modal-wrapper" >
        <div className="modal" onClick={e => e.stopPropagation()}>
          <div className="modal-box">
            <div className="modal-header" style={{backgroundColor:backgroundColorTitle}}>
              <h4 className="modal-text">{header}</h4>
              {closeButton &&  
              <button type="button" className="modal-close" onClick={onClick} >
                <svg viewBox="0 0 16 16" width="16" height="16">
                  <path d="m9.3 8 6.1-6.1c.4-.4.4-.9 0-1.3s-.9-.4-1.3 0L8 6.7 1.9.6C1.6.3 1 .3.6.6c-.3.4-.3 1 0 1.3L6.7 8 .6 14.1c-.4.4-.3.9 0 1.3l.1.1c.4.3.9.2 1.2-.1L8 9.3l6.1 6.1c.4.4.9.4 1.3 0s.4-.9 0-1.3L9.3 8z" />
                </svg>
              </button>
    
              }
            </div>
            <div className="modal-content" style={{backgroundColor:backgroundColorText}}>
              <p className="modal-content-text">
                {text}
              </p>

              <div className="modal-first">
                <div className="button-wrapper">   
                  {actions}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </>
      
    );
  }
}

Modal.protoTypes = {
  header: PropTypes.string, 
  text: PropTypes.string, 
  actions: PropTypes.func, 
  closeButton: PropTypes.func, 
  onClick: PropTypes.func, 
  backgroundColorTitle: PropTypes.string, 
  backgroundColorText: PropTypes.string
}
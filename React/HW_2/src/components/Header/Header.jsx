import React from "react";
import PropTypes from "prop-types"
import img from '../Header/img/heart-icon.svg'
import classNames from "classnames"
import './Header.scss'
import imgSrc from '../Header/logo.png';
export default class Header extends React.Component{
    render(){
        const {count,countBuy,} = this.props
        return(
        
        <>
            <div className="header">    
                    <img className="header-logo" src={imgSrc} alt="" />  
                    <div className="header-button" >
                        <div className="header-favorite">
                            
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16.2 8.1c-.2.1-.3.1-.4 0-4-5.7-11.6-1.7-11.6 3.8 0 4.8 6.1 8.7 11 14.1 0 0 0 .1.1.1.4.4 1 .3 1.4-.1 4.9-5.4 11-9.2 11-14.1.1-5.5-7.2-9.5-11.5-3.8z"/></svg>
                            <p className="count-favorite">{count}</p>
                        </div>
                        <div className="header-bag">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"> <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z"/> </svg>
                            <p className="count-buy">{countBuy}</p>
                        </div>
                        
                    </div>
            </div>
            
        </>
            
        
        )
    }
}
Header.protoTypes={
    countFavorite: PropTypes.number,
    countBuy: PropTypes.number,

}

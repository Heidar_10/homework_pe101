import React from "react";
import "./Button.scss"

export default class Button extends React.Component{
    render(){
        const {onClick, text, backgroundColor,addToOrder} = this.props;

        return(
            <div>
                <button className="button-modal" style={{backgroundColor:backgroundColor}} onClick={onClick} onAdd={this.addToOrder} >{text}</button>
            </div>             
        )
    }
}
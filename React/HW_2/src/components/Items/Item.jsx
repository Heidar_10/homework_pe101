import React from 'react'
import './Items'
import Button from '../Button/Button';
import PropTypes from 'prop-types';

export class Item extends React.Component {
  state = {colorStar:'black'}
  onClickStar=()=>{
      const color = this.state.colorStar
      console.log(color);
      if (color === "black") {
        this.setState({
           colorStar: "yellow"
           
        })
      }
      else if(color === 'yellow') {
        this.setState({
          colorStar: "black"
       })
      }
      

  }
  

  render() {
    const {onClick ,img,name,price,color, article,openSecondModal,handelFavorite,onFavorite } = this.props;
    const {colorStar} = this.state;
    console.log();
    return (

        <div className="item">
            <img className='item__img' src={'./img/'+ this.props.item.img} alt="" />
            <h2 className='item__name'>{this.props.item.name}</h2>
            <b className='item__price'>{this.props.item.price}</b>
            <p className='item__color'>Color: {this.props.item.color}</p>
            <p className='item__article'>Article: {this.props.item.article}</p>
            <div className='star'>
                <svg xmlns="http://www.w3.org/2000/svg"  onClick={()=>{this.onClickStar();this.props.onFavorite(this.props.item)}}  fill={colorStar} width="24" height="24" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>
            </div>
            <Button onClick = {()=>{this.props.openSecondModal(this.props.item)}} text={'Buy'} backgroundColor={'green'}/>
        </div>
        
    )
  }
}

Item.propTypes = {
  onClick: PropTypes.func ,
  img:PropTypes.object,
  name:PropTypes.string,
  price:PropTypes.number,
  color:PropTypes.string, 
  article:PropTypes.string,
  openSecondModal:PropTypes.func
};
export default Item
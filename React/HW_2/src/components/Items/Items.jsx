import React from "react"
import './Items.scss'

// import Data from "../../data.json"
import Item from "./Item"
export default class Items extends React.Component {
  state = { sendRequest:[]}
  
  componentDidMount(){
   fetch("data.json")
   .then(response =>response.json())
   .then(result => this.setState({sendRequest: result}) )
  }


render() {
    const {openSecondModal, item}= this.props
    const {sendRequest} = this.state
    return (
     <main className="main">
        {sendRequest.map(el =>(
                <>
                  <Item item={el} openSecondModal = {this.props.openSecondModal} onAdd = {this.props.onAdd} onFavorite={this.props.onFavorite} />   
                                 
                </>
        ))}
        
     </main>
    )
  }
}


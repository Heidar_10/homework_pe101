// import browserSync from "browser-sync";
// import gulp from "gulp";
// const { src, dest, watch, series, parallel } = gulp;
// const bsServer = browserSync.create();

// import dartSass from "sass";
// import gulpSass from "gulp-sass";
// const sass = gulpSass(dartSass);


// function styles() {
//     return src("./src/scsss/style.scss").
//     pipe(sass().on('error', sass.logError))
//     .pipe(dest("./dist/css/"))
//     .pipe(bsServer.reload({stream:true}));
// }

// function scripts() {
//     return src("./src/js/**/*.js")
//         .pipe(dest("./dist/js/"))
//         .pipe(bsServer.reload({ stream: true }));
// }


// function watcher(){
//     watch("./src/scsss/**/*.scss", styles);
//     watch("*.html").on("change", bsServer.reload);
//     watch("./src/js/*.js", scripts);
// }


// function serve() {
//     bsServer.init({
//         server: {
//             baseDir: "./",
//             // browser: "google chrome"        
//         },
//     });
// }

// export const dev = series(styles, scripts, parallel(serve, watcher));
// // добавить переменную build без  serve, watcher

import gulp from "gulp";
const {src, dest, watch, series, parallel} = gulp;

import clean from "gulp-clean";

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

import autoprefixer from "gulp-autoprefixer";

import cleanScc from "gulp-clean-css";

import minifyJs from "gulp-js-minify";

import concat from "gulp-concat";

import browserSync from "browser-sync"
const bsServer = browserSync.create();

import imageMin from "gulp-imagemin";


function deleteDist() {
    return src("./dist/*", {"allowEmpty" : true}).pipe(clean());
}
function styles() {
    return src("./src/styles/*.scss")
        .pipe(autoprefixer( ["last 15 versions", "> 1%", "ie 8", "ie 7"], {
                cascade: true,
            }
        ))
        .pipe(sass().on('error', sass.logError))
        .pipe(concat("./styles.min.css"))
        .pipe(dest("./dist/css/"))
        .pipe(bsServer.reload({stream : true}));
}

function scripts() {
    return src("./src/js/*.js")
        .pipe(concat("./js/script.min.js"))
        .pipe(minifyJs())
        .pipe(dest("./dist/"));
    
}

function images() {
    return src("./src/img/*")
        .pipe(imageMin())
        .pipe(dest("dist/images"))

}

function serve() {
    bsServer.init({
        server: {
            baseDir: "./",
        }
    });
}

function watcher() {
    watch("./src/styles/**/*.scss",styles);
    watch("*.html").on("change", bsServer.reload);
    watch("./src/js/*.js").on("change", series(scripts,bsServer.reload));
    watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}").on(
        "change",
        series(images, bsServer.reload)
    );
}


export const build = series(deleteDist,parallel(styles,scripts,images));

export const dev = series(styles, scripts, images, parallel(serve, watcher));